# Tonka

Ein TONuino KAtalog.

## CSV Spec für App Import

Um die App nutzen zu können, muss der Inhalt der Tonuino SD Kate als CSV importiert werden. 

Die CSV Datei muss eine Zeile pro Datei auf der SD Karte haben. Sie muss folgende Spalten haben


| Spalte           | Spec                                     | Darf leer sein | Beschreibung                                                                                                                                                 | 
|------------------|---------------------------------------|---------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------|
| directoryNumber  | Muss ein Integer zwischen 1-99 sein      | Nein           | Die Nummer des Ordners auf der SD Karte                                                                                                                      |
| directoryName    | Ein beliebiger String                    | Ja             | Titel des Ordners auf der SD Karte                                                                                                                           |
| fileNumber       | Muss ein Integer zwischen 1-255 sein     | Nein           | Die Nummer der Datei auf der SD Karte (Der Dateiname).                                                                                                       |
| title            | Ein beliebiger String                    | Ja             | Ein String, der die Datei beschreibt.                                                                                                                        |
| subdirectoryName | Ein beliebiger String                    | Ja             | Falls die Datei zu einem Album (o. Ä.) gehört, der Name des Albums. Aufeinanderfolgende Dateien mit identischen subdirectoryName werden in der App visuell grupiert. |
| tags             | 0 oder mehr Strings, getrennt durch `;;` | ja             | Eine Liste von Tags, hier können beliebige Details eingefügt werden, zb eine Altersempfehlung oder `Music` etc...                                          |  
| runtime          | Ein positiver Integer                    | Ja             | Die Laufzeit der Datei in s.                                                                                                                                 |

Die erste Zeile der CSV Datei muss die Spaltennamen enthalten. Die Reihenfolge der Spalten ist beliebig. Die Reihenfolge der Zeilen (bis auf den Header) ist auch beliebig.


## Das Script make_sd.py

Die CSV Datei für die Tonka App kann auf viele Arten erstellt werden. Hier möchte ich meine vorstellen.

### Die Idee

Ich möchte auf keinen Fall Dateien auf der Festplatte und einen Katalog (zb Excel) getrennt pflegen. Deswegen werden die Audiodateien direkt korrekt benannt und nummeriert.

### Mediathek

Damit das Pythonscript funktioniert, müssen die Audiodateien korrekt angeordnet werden

```
├── 01 Benjamin Blümchen
│   ├── 001 Benjamin Blümchen Folge 1.mp3
│   └── 002 Benjamin Blümchen Folge 2.mp3
├── 02 Tiergeräusche
│   └── 001 Bellen.mp3
├── 03 Musik
    ├── 001 Mein cooles Album
    │   ├── 01 Track 1.mp3
    │   └── 02 Track 2.mp3
    └── 003 Partysongs
        ├── 01 Track 1.mp3
        └── 02 Track 2.mp3
```

*  Jeder Ordner/Jede Datei, die nicht mit einer Zahl beginnt, wird ignoriert. So ist sichergestellt, dass sich die Nummerierung nicht aus versehen irgendwann ändert. Als Nummerierung wird 1, 01 etc. akzeptiert. 
*  Es darf bis zu 99 Ordner geben. In jedem dürfen bis zu 255 mp3 Dateien liegen. 
*  Es können Unterordner angelegt werden. Der Name wird als Albumname verwendet. Die Nummerierung der Unterordner muss genug "Platz" für alle Songs lassen. Im Beispiel oben haben die Partysongs die Nummer 3, da Mein cooles Album 2 Dateien enthält.  

### Script aufrufen

`python3 --source /pfad/zur/mediathek [--out /pfad/zur/ausgabe]`. 

Das Script kopiert die Dateien nicht, sondern erstellt Symlinks und ist somit sehr schnell.

### Ausgabe

Das Script erstellt diese Ordnerstruktur

```
├── 01
│   ├── 001.mp3
│   └── 002.mp3
├── 02
│   └── 001.mp3
├── 03
    ├── 001.mp3
    ├── 002.mp3
    ├── 003.mp3
    └── 004.mp3
```
und eine CSV, um sie mit der Tonka App zu nutzen.

```
directoryNumber,fileNumber,directoryName,subdirectoryName,title,tags,runtime
1,1,Benjamin Blümchen,,Benjamin Blümchen Folge 1,5J;;Favoriten,2700
1,2,Benjamin Blümchen,,Benjamin Blümchen Folge 2,,2650
2,1,Tiergeräusche,,Bellen,,,15
3,1,Musik,Mein cooles Album,Track 1,Musik,185
3,2,Musik,Mein cooles Album,Track 2,Musik,136
3,3,Musik,Partysongs,Track 1,Musik,
3,4,Musik,Partysongs,Track 2,Musik,
```


## Todos

* Tags aus Dateinamen lesen (zb `001 Frau Holle [5J, Favoriten].mp3`)
* mp3 Laufzeit auslesen und in der App anzeigen
  * Wenn alle Dateien eines Albums oder eines Ordners dasselbe Tag haben, hat stattdessen der Ordner das Tag.
* Optionen
  * Initial alles ausgeklappt oder eingeklappt
  * Tonuino Version
  * Anzahl der Titel in einem Album anzeigen
 
 #### Ein paar Ideen
 * Karten ranhalten und Song/Album anzeigen.
 * Automatisch csv Dateien aus der Cloud laden.
# Tonka

Ein TONuino KAtalog.

## CSV Spec

Um die App nutzen zu können, muss der Inhalt der Tonuino SD Kate als CSV importiert werden. 

Die CSV Datei muss eine Zeile pro Datei auf der SD Karte haben. Sie muss folgende Spalten haben


| Spalte           | Spec                                     | Darf leer sein | Beschreibung                                                                                                                                                 | 
|------------------|---------------------------------------|---------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------|
| directoryNumber  | Muss ein Integer zwischen 1-99 sein      | Nein           | Die Nummer des Ordners auf der SD Karte                                                                                                                      |
| directoryName    | Ein beliebiger String                    | Ja             | Titel des Ordners auf der SD Karte                                                                                                                           |
| fileNumber       | Muss ein Integer zwischen 1-255 sein     | Nein           | Die Nummer der Datei auf der SD Karte (Der Dateiname).                                                                                                       |
| title            | Ein beliebiger String                    | Ja             | Ein String, der die Datei beschreibt.                                                                                                                        |
| subdirectoryName | Ein beliebiger String                    | Ja             | Falls die Datei zu einem Album gehört, der Name des Albums. Aufeinanderfolgende Dateien mit identischen subdirectoryName werden in der App visuell grupiert. |
| tags             | 0 oder mehr Strings, getrennt durch `;;` | ja             | Eine Liste von Tags, hier können beliebige Details eingefügt werden, zb eine Altersempfehlung oder `Music` etc...                                          |  
| runtime          | Ein positiver Integer                    | Ja             | Die Laufzeit der Datei in s.                                                                                                                                 |

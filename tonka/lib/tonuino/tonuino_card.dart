// The play modes supported by this app. Note that the regular variants of most
// modes (without from-to) aren't supported, since that would be extra work
// without any benefit.
import 'dart:typed_data';

import 'package:crypto/crypto.dart';
import 'package:tonka/data/catalog.dart';
import 'dart:convert';

import 'hash.dart';

enum PlayMode {
  // Plays the specified track. Einzelmodus
  SINGLE,
  // Plays an entire directory. Saves the progress. Hoerbuchmodus
  AUDIO_BOOK,
  // Play the selected tracks in order.
  ALBUM,
  // Plays the selected tracks in random order.
  PARTY,
  // Plays one random track out of the selected tracks, Hoerspielmodus
  AUDIO_DRAMA
}

enum ModifierType {
  ADMIN,
  SLEEP_TIMER,
  FREEZE_DANCE,
  LOCK,
  TODDLER_MODE,
  KINDERGARDEN_MODE,
  REPEAT_SINGLE,
  FEEDBACK
}

enum TonuinoVersion { TONUINO_2_0, TONUINO_2_1 }

final Uint8List CARD_COOKIE = Uint8List.fromList([0x13, 0x37, 0xb3, 0x47]);

bool _inRange(int? i, int lo, int hi) => (i != null && i >= lo && i <= hi);

bool _validFolder(int? f) => _inRange(f, 1, 99);

bool _validFile(int? f) => _inRange(f, 1, 255);

bool _verifyInputs(TonuinoCard card) {
  if (card.mode == null) return false;
  switch (card.mode) {
    case null:
      return false;
    case PlayMode.SINGLE:
      return _validFolder(card.folder) && _validFile(card.single);
    case PlayMode.ALBUM:
      return _validFolder(card.folder) &&
          (_validFile(card.from) == _validFile(card.to));
    case PlayMode.PARTY:
      return _validFolder(card.folder) &&
          (_validFile(card.from) == _validFile(card.to));
    case PlayMode.AUDIO_BOOK:
      return _validFolder(card.folder);
    case PlayMode.AUDIO_DRAMA:
      return _validFolder(card.folder) &&
          (_validFile(card.from) == _validFile(card.to));
  }
}

int _versionCode(TonuinoVersion version) {
  switch (version) {
    case TonuinoVersion.TONUINO_2_0:
      return 1;
    case TonuinoVersion.TONUINO_2_1:
      return 2;
  }
}

int _folderByte(int folder) {
  return folder;
}

int _modeByte(PlayMode mode, bool fromToSpecialMode) {
  switch (mode) {
    case PlayMode.SINGLE:
      return 4;
    case PlayMode.ALBUM:
      return fromToSpecialMode ? 8 : 2;
    case PlayMode.PARTY:
      return fromToSpecialMode ? 9 : 3;
    case PlayMode.AUDIO_BOOK:
      return 5;
    case PlayMode.AUDIO_DRAMA:
      return fromToSpecialMode ? 7 : 1;
  }
}

Uint8List _specialBytes(PlayMode mode, int? single, int? from, int? to) {
  if (mode == PlayMode.SINGLE) {
    return Uint8List.fromList([single ?? 0, 0]);
  }
  if ([PlayMode.ALBUM, PlayMode.PARTY, PlayMode.AUDIO_DRAMA].contains(mode)) {
    return Uint8List.fromList([from ?? 0, to ?? 0]);
  }
  return Uint8List(0);
}

class TonuinoCard {
  TonuinoCard(this.mode,
      {required this.folder, this.single, this.from, this.to});

  TonuinoCard.folderMode({required this.mode, required Directory directory})
      : folder = directory.number,
        directoryHash = hashString(directory.name);

  TonuinoCard.albumMode({required this.mode, required Album album})
      : folder = album.directory.number,
        from = album.files.first.number,
        to = album.files.last.number,
        directoryHash = hashString(album.directory.name),
        albumOrTitleHash = hashString(album.name);

  TonuinoCard.singleMode({required AudioFile file})
      : mode = PlayMode.SINGLE,
        folder = file.directory.number,
        single = file.number,
        directoryHash = hashString(file.directory.name),
        albumOrTitleHash = hashString(file.name);

  TonuinoCard.fromBytes(this.bytes,
      {this.directoryHash, this.albumOrTitleHash});

  PlayMode? mode;
  int? folder;
  int? single;
  int? from;
  int? to;
  TonuinoVersion version = TonuinoVersion.TONUINO_2_1;

  // The actual Tonuino payload.
  Uint8List? bytes;

  // We always store the directoryHash
  Uint8List? directoryHash;

  // We store the album hash for from-to type of cards. We store the title hash
  // for single-title cards. We store nothing for the rest.
  Uint8List? albumOrTitleHash;

  Uint8List makeTonuinoCode() {
    if (bytes != null && bytes!.isNotEmpty) return bytes!;
    _verifyInputs(this);

    final fromToSpecialMode = from != null && to != null;

    var builder = new BytesBuilder();
    builder.add(CARD_COOKIE);
    builder.addByte(_versionCode(version));

    builder.addByte(_folderByte(folder!));
    builder.addByte(_modeByte(mode!, fromToSpecialMode));
    builder.add(_specialBytes(mode!, single, from, to));
    while (builder.length < 16) builder.addByte(0);
    bytes = builder.takeBytes();
    return bytes!;
  }
}

TonuinoCard makeModifierCard(
    {required ModifierType modifierType, int? sleepMinutes}) {
  if (modifierType == ModifierType.SLEEP_TIMER &&
      !_inRange(sleepMinutes, 1, 255)) {
    throw Exception("Can't write a sleep timer card without a valid time.");
  }
  var builder = new BytesBuilder();
  builder.add(CARD_COOKIE);
  builder.addByte(_versionCode(TonuinoVersion.TONUINO_2_1));
  builder.addByte(0); // folder byte
  builder.addByte(_modifierByte(modifierType));
  builder.addByte(sleepMinutes ?? 0);
  while (builder.length < 16) builder.addByte(0);
  return TonuinoCard.fromBytes(builder.takeBytes());
}

int _modifierByte(ModifierType modifierType) {
  switch (modifierType) {
    case ModifierType.ADMIN:
      return 0;
    case ModifierType.SLEEP_TIMER:
      return 1;
    case ModifierType.FREEZE_DANCE:
      return 2;
    case ModifierType.LOCK:
      return 3;
    case ModifierType.TODDLER_MODE:
      return 4;
    case ModifierType.KINDERGARDEN_MODE:
      return 5;
    case ModifierType.REPEAT_SINGLE:
      return 6;
    case ModifierType.FEEDBACK:
      return 7;
  }
}


import 'dart:convert';
import 'dart:typed_data';

import 'package:crypto/crypto.dart';

Uint8List? hashString(String? s) {
  if (s == null) return null;
  if (s.isEmpty) return null;
  return Uint8List.fromList(md5.convert(utf8.encode(s)).bytes);
}

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:tonka/data/hive_type_adapters.dart';
import 'package:tonka/ui/main/main_view.dart';
import 'package:tonka/data/csv_row_data.dart';



void main() async {
  await Hive.initFlutter();
  Hive.registerAdapter(CsvRowDataAdapter());
  Hive.registerAdapter(DurationHiveTypeAdapter());
  await Hive.openBox<CsvRowData>('repo');

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Tonka',
      theme: ThemeData(
          primarySwatch: Colors.blueGrey,
          backgroundColor: Colors.white60
      ),
      home: MyHomePage(),
    );
  }
}


class MyHomePage extends StatelessWidget {
  MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MainView();
  }
}
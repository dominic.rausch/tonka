// TODO: extract all strings to a localizeable format.void

const String albumModeTooltip = "Alle Titel in Reihenfolge abspielen";
const String partyModeTooltip = "Alle Titel in zufälliger Reihenfolge abspielen";
const String audioBookTooltip = "Alle Titel in Reihenfolge abspielen und Fortschritt speichern";
const String audioDramaTooltip = "Einen zufälligen Titel abspielen";
import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:nfc_manager/nfc_manager.dart';
import 'package:nfc_manager/platform_tags.dart';
import 'package:tonka/tonuino/tonuino_card.dart';

final MIFARE_CLASSIC_KEY =
    Uint8List.fromList([0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]);

writeNfcDialog(
    {required BuildContext context, required TonuinoCard card}) async {
  bool isAvailable = await NfcManager.instance.isAvailable();
  if (!isAvailable) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(
          "NFC muss aktiviert sein, muss eine Karte zu beschreiben. Eventuell hat dieses Gerät hat keine NFC Funktion"),
    ));
    return;
  }
  ScaffoldMessenger.of(context).removeCurrentSnackBar();

  var bytes = card.makeTonuinoCode();

  Future nfcFuture =
      NfcManager.instance.startSession(onDiscovered: (NfcTag tag) async {
    debugPrint(jsonEncode(tag.data));
    var mifareClassic = MifareClassic.from(tag);
    var mifareUl = MifareUltralight.from(tag);
    var nfcA = NfcA.from(tag);
    try {
      if (mifareClassic != null) {
        debugPrint("Writing bytes ${bytes.toString()} to mifare classic.");
        await mifareClassic.authenticateSectorWithKeyA(
            sectorIndex: 1, key: MIFARE_CLASSIC_KEY);
        await mifareClassic.writeBlock(blockIndex: 4, data: bytes);
        if (card.directoryHash != null)
          await mifareClassic.writeBlock(
              blockIndex: 5, data: card.directoryHash!);
        if (card.albumOrTitleHash != null)
          await mifareClassic.writeBlock(
              blockIndex: 6, data: card.albumOrTitleHash!);
      } else if (mifareUl != null) {
        debugPrint("Writing bytes ${bytes.toString()} to mifare ultralight.");
        await mifareUl.writePage(pageOffset: 8, data: bytes.sublist(0, 4));
        await mifareUl.writePage(pageOffset: 9, data: bytes.sublist(4, 8));
        await mifareUl.writePage(pageOffset: 10, data: bytes.sublist(8, 12));
        await mifareUl.writePage(pageOffset: 11, data: bytes.sublist(12, 16));
      } else if (nfcA != null) {
        debugPrint("Writing bytes ${bytes.toString()} to NfcA.");
        await nfcA.transceive(
            data: Uint8List.fromList([0xA2, 8] + bytes.sublist(0, 4)));
        await nfcA.transceive(
            data: Uint8List.fromList([0xA2, 9] + bytes.sublist(4, 8)));
        await nfcA.transceive(
            data: Uint8List.fromList([0xA2, 10] + bytes.sublist(8, 12)));
        await nfcA.transceive(
            data: Uint8List.fromList([0xA2, 11] + bytes.sublist(12, 16)));
      } else {
        debugPrint("Unsupported Tag");
      }
      ScaffoldMessenger.of(context).hideCurrentSnackBar();
    } catch (e) {
      ScaffoldMessenger.of(context).hideCurrentSnackBar();
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Fehler: ' + e.toString()),
        duration: Duration(seconds: 5),
      ));
      return;
    } finally {
      NfcManager.instance.stopSession();
    }
  });

  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    content: Text('NFC Tag ans Telefon halten.'),
    duration: Duration(seconds: 10),
    action: SnackBarAction(
      label: 'Abbrechen',
      onPressed: () {
        ScaffoldMessenger.of(context).hideCurrentSnackBar();
        NfcManager.instance.stopSession();
      },
    ),
  ));
  await nfcFuture;
}

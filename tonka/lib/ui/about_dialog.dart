import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:url_launcher/url_launcher.dart';

void showAboutTonkaDialog(BuildContext context) async {
  PackageInfo packageInfo = await PackageInfo.fromPlatform();

  final String appVersion = "${packageInfo.version}+${packageInfo.buildNumber}";
  showAboutDialog(context: context, applicationVersion: appVersion, children: [
    new RichText(
      text: new TextSpan(
        children: [
          new TextSpan(
            text: 'Anleitung auf ',
            style: new TextStyle(color: Colors.black),
          ),
          new TextSpan(
            text: 'https://gitlab.com/dominic.rausch/tonka/-/blob/master/README.md',
            style: const TextStyle(color: Colors.blue),
            recognizer: new TapGestureRecognizer()
              ..onTap = () { launch('https://gitlab.com/dominic.rausch/tonka/-/blob/master/README.md');
              },
          ),
        ],
      ),
    ),
  ]);
}

import 'package:flutter/material.dart';

import 'package:tonka/data/catalog.dart';
import 'package:tonka/ui/main/directory_sliver.dart';

class CatalogListView extends StatelessWidget {
  final Catalog _catalog;

  CatalogListView(this._catalog, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => CustomScrollView(
      slivers: _catalog.directories
          .map((e) => DirectorySliver(e))
          .toList(growable: false));
}

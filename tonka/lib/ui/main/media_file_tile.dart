import 'package:flutter/material.dart';
import 'package:tonka/data/catalog.dart';
import 'package:tonka/tonuino/tonuino_card.dart';
import 'package:tonka/ui/write_nfc_popup.dart';

class MediaFileTile extends StatelessWidget {
  final AudioFile audioFile;
  String? runtime;

  MediaFileTile({Key? key, required this.audioFile}) : super(key: key) {
    runtime = makeRuntimeString();
  }

  String makeTitle() {
    return audioFile.name;
  }

  String? makeRuntimeString() {
    if (audioFile.runtime == null) {
      return null;
    }
    Duration dur = audioFile.runtime!;
    final hours = dur.inHours;
    if (hours == 0) {
      return "${dur.inMinutes}m ${dur.inSeconds.remainder(60)}s";
    } else {
      return "${hours}h ${dur.inMinutes.remainder(60)}m";
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Text(
          audioFile.number.toString(),
          style: Theme
              .of(context)
              .textTheme
              .caption,
        ),
      ]),
      title: Text(
        makeTitle(),
        style: Theme
            .of(context)
            .textTheme
            .bodyText2,
      ),
      subtitle: audioFile.tags == null || audioFile.tags!.isEmpty
          ? null
          : Wrap(
          children: audioFile.tags!
              .map<Widget>((tag) =>
              Container(
                margin: EdgeInsets.only(left: 10),
                child: Chip(label: Text(tag)),
              ))
              .toList(growable: false)),
      trailing: runtime == null
          ? null
          : Text(runtime!, style: Theme
          .of(context)
          .textTheme
          .bodyText2),
      onTap: () =>
          writeNfcDialog(
              context: context, card: TonuinoCard.singleMode(file: audioFile)),
    );
  }
}

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:tonka/data/catalog.dart';
import 'package:tonka/data/load_csv.dart';
import 'package:tonka/ui/about_dialog.dart';
import 'package:tonka/ui/main/catalog_list_view.dart';
import 'package:tonka/ui/main/search_bar.dart';
import '../../data/csv_row_data.dart';
import '../modifier_cards_view.dart';
import '../update_cards_view.dart';

class MainView extends StatefulWidget {
  MainView({Key? key}) : super(key: key);

  @override
  _MainViewState createState() => _MainViewState();
}

enum MainMenuOptions { reload, about, modifier, updateCards }

class _MainViewState extends State<MainView> {
  final catalogStream = new StreamController<Catalog>();
  Catalog _fullCatalog = Catalog.empty();

  String searchTerm = "";
  Catalog? _filteredCatalog;

  _MainViewState() {
    var box = Hive.box<CsvRowData>('repo');
    var updateCatalog = () {
      _fullCatalog = Catalog.fromMediaFiles(box.values.toList());
      _filteredCatalog = null;
      catalogStream.add(_fullCatalog);
    };

    updateCatalog();
    box.listenable().addListener(updateCatalog);
  }

  void renderImportLog(String s) {
    showModalBottomSheet(
      context: context,
      builder: (context) => Container(
        padding: EdgeInsets.all(8),
        child: Text(
          s,
          maxLines: 100,
        ),
      ),
    );
  }

  void loadFile(BuildContext context) async {
    ImportResult import = await loadCsv();

    if (import.csvRows.isNotEmpty) {
      await Hive.box<CsvRowData>('repo').clear();
      Hive.box<CsvRowData>('repo').addAll(import.csvRows);
      setState(() {});
    }
    renderImportLog(import.importLog);
  }

  void pushModifierRoute(BuildContext context) {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => ModifierCardsView()));
  }

  List<Widget> _buildPopupMenu(BuildContext context) => [
        PopupMenuButton<MainMenuOptions>(
          onSelected: (MainMenuOptions selected) {
            if (selected == MainMenuOptions.reload) loadFile(context);
            if (selected == MainMenuOptions.about)
              showAboutTonkaDialog(context);
            if (selected == MainMenuOptions.modifier)
              pushModifierRoute(context);
            if (selected == MainMenuOptions.updateCards)
              pushUpdateCardsRoute(context, _fullCatalog);
          },
          itemBuilder: (BuildContext context) =>
              <PopupMenuEntry<MainMenuOptions>>[
            const PopupMenuItem<MainMenuOptions>(
                value: MainMenuOptions.reload, child: Text("CSV Datei laden")),
            const PopupMenuItem<MainMenuOptions>(
                value: MainMenuOptions.modifier,
                child: Text("Modifikationskarten")),
            if (_fullCatalog != null && _fullCatalog.directories.isNotEmpty)
              const PopupMenuItem<MainMenuOptions>(
                value: MainMenuOptions.updateCards,
                child: Text("Karten aktualisieren"),
              ),
            const PopupMenuItem<MainMenuOptions>(
                value: MainMenuOptions.about, child: Text("Über"))
          ],
        )
      ];

  AppBar _buildAppBar(BuildContext context) {
    return AppBar(
        title: const Text('Tonuino Katalog'),
        actions: _buildPopupMenu(context));
  }

  void _search(String query) async {
    query = query.trim().toLowerCase();
    if (query == searchTerm) return;
    debugPrint("Search for '$query'");

    var newCatalog = _fullCatalog;
    if (query.startsWith(searchTerm) && _filteredCatalog != null) {
      newCatalog = await _filteredCatalog!.filteredCopy(query);
    } else {
      newCatalog = await _fullCatalog.filteredCopy(query);
    }

    _filteredCatalog = newCatalog;
    catalogStream.add(newCatalog);
    searchTerm = query;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: _buildAppBar(context),
        body: _fullCatalog.directories.isEmpty
            ? Center(
                child: OutlinedButton(
                  child: Text("CSV Datei laden"),
                  onPressed: () => loadFile(context),
                ),
              )
            : Stack(fit: StackFit.loose, children: [
                StreamBuilder(
                    stream: catalogStream.stream,
                    builder: (context, AsyncSnapshot<Catalog?> snapshot) {
                      if (snapshot.hasData)
                        return CatalogListView(snapshot.data!);
                      else
                        return CircularProgressIndicator();
                    }),
                Positioned(
                  bottom: 0,
                  right: 0,
                  child: AnimatedSearchBar(onSubmitted: _search),
                ),
              ]));
  }

  @override
  void dispose() {
    catalogStream.close();
    super.dispose();
  }
}

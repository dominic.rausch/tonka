import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:tonka/data/catalog.dart';
import 'package:tonka/strings.dart';
import 'package:tonka/tonuino/tonuino_card.dart';
import 'package:tonka/ui/main/media_file_tile.dart';
import 'package:tonka/ui/write_nfc_popup.dart';

class AlbumCard extends StatelessWidget {
  final Album _album;

  const AlbumCard(this._album, {Key? key}) : super(key: key);

  void _makeCard(BuildContext context, PlayMode mode) => writeNfcDialog(
      context: context,
      card: TonuinoCard.albumMode(
        mode: mode,
        album: _album,
      ));

  Widget _makeButtonRow(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        mainAxisSize: MainAxisSize.max,
        children: [
          Tooltip(
            message: albumModeTooltip,
            child: OutlinedButton(
                onPressed: () => _makeCard(context, PlayMode.ALBUM),
                child: Text("Album")),
          ),
          Tooltip(
            message: partyModeTooltip,
            child: OutlinedButton(
                onPressed: () => _makeCard(context, PlayMode.PARTY),
                child: Text("Party")),
          ),
          Tooltip(
            message: audioDramaTooltip,
            child: OutlinedButton(
                onPressed: () => _makeCard(context, PlayMode.AUDIO_DRAMA),
                child: Text("Hörspiel")),
          ),
        ],
      );

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        side: BorderSide(color: Colors.black12, width: 1),
        borderRadius: BorderRadius.circular(10),
      ),
      margin: const EdgeInsets.fromLTRB(8, 0, 8, 8),
      color: Colors.blueGrey[30],
      elevation: 0,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(16, 4, 0, 8),
        child: ExpandableNotifier(
          initialExpanded: false,
          child: ExpandablePanel(
            theme: ExpandableThemeData(
                headerAlignment: ExpandablePanelHeaderAlignment.center),
            collapsed: _makeButtonRow(context),
            header: Text(
              _album.name,
              style: Theme.of(context).textTheme.subtitle2,
              textAlign: TextAlign.left,
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
            ),
            expanded: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[_makeButtonRow(context)] +
                  _album.files
                      .map((audioFile) => MediaFileTile(
                            audioFile: audioFile,
                          ))
                      .toList(),
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'package:tonka/strings.dart';
import 'package:tonka/tonuino/tonuino_card.dart';
import 'package:tonka/ui/write_nfc_popup.dart';

import '../../data/catalog.dart';
import 'album_card.dart';
import 'media_file_tile.dart';

class DirectorySliver extends StatelessWidget {
  final Directory _directory;
  final expandableController = new ExpandableController(initialExpanded: false);
  final headerKey = new GlobalKey();

  DirectorySliver(this._directory, {Key? key}) : super(key: key);

  String _headerText() {
    return "(${_directory.number}) - ${_directory.name}";
  }

  void makeCard(BuildContext context, PlayMode mode) {
    writeNfcDialog(
        context: context,
        card: TonuinoCard.folderMode(mode: mode, directory: _directory));
  }

  Widget _makeButtonRow(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Tooltip(
              message: albumModeTooltip,
              child: OutlinedButton(
                  onPressed: () => makeCard(context, PlayMode.ALBUM),
                  child: Text("Album"))),
          Tooltip(
              message: partyModeTooltip,
              child: OutlinedButton(
                  onPressed: () => makeCard(context, PlayMode.PARTY),
                  child: Text("Party"))),
          Tooltip(
              message: audioDramaTooltip,
              child: OutlinedButton(
                  onPressed: () => makeCard(context, PlayMode.AUDIO_DRAMA),
                  child: Text("Hörspiel"))),
          Tooltip(
              message: audioBookTooltip,
              child: OutlinedButton(
                  onPressed: () => makeCard(context, PlayMode.AUDIO_BOOK),
                  child: Text("Hörbuch"))),
        ],
      );

  Widget _makeHeader(BuildContext context) => GestureDetector(
        onTap: () {
          expandableController.toggle();
        },
        child: Container(
          // height: 45.0,
          key: headerKey,
          color: Colors.blueGrey,
          padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 12),
          alignment: Alignment.centerLeft,
          child: Text(
            _headerText(),
            style: Theme.of(context)
                .textTheme
                .subtitle2!
                .copyWith(color: Colors.white),
          ),
        ),
      );

  Widget _makeContentRow(BuildContext context, int index) {
    if (index == 0) return _makeButtonRow(context);
    index--;
    if (_directory.content[index].file != null) {
      return MediaFileTile(
        audioFile: _directory.content[index].file!,
      );
    } else if (_directory.content[index].dir != null) {
      return AlbumCard(_directory.content[index].dir!);
    } else {
      throw Exception(
          "Trying to render a directory but found a null entry in it.");
    }
  }

  @override
  Widget build(BuildContext context) => SliverStickyHeader(
      header: _makeHeader(context),
      sliver: SliverToBoxAdapter(
          child: ExpandablePanel(
              controller: expandableController,
              theme: const ExpandableThemeData(
                  hasIcon: true, tapHeaderToExpand: true),
              // header: const Divider(),// _directoryStatsRow(context),
              collapsed: Container(),
              expanded: ValueListenableBuilder<bool>(
                  valueListenable: expandableController,
                  builder: (context, isExpanded, child) => isExpanded
                      ? ListView.builder(
                          shrinkWrap: true,
                          itemBuilder: _makeContentRow,
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: _directory.content.length + 1,
                        )
                      : Container()))));
}

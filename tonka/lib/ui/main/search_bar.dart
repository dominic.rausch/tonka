import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AnimatedSearchBar extends StatefulWidget {
  final controller = TextEditingController();
  final Duration _animationDuration = Duration(milliseconds: 150);
  final Function(String)? onSubmitted;

  AnimatedSearchBar({Key? key, this.onSubmitted}) : super(key: key);

  @override
  _AnimatedSearchBarState createState() => _AnimatedSearchBarState();
}

class _AnimatedSearchBarState extends State<AnimatedSearchBar> {
  bool isExpanded = false;
  final textKey = UniqueKey();
  static const double padding = 8.0;


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(padding),
      child: AnimatedContainer(
        duration: widget._animationDuration,
        alignment: Alignment.bottomCenter,
        height: isExpanded ? 50 : 60,
        width:
        isExpanded ? MediaQuery
            .of(context)
            .size
            .width - 2 * padding  : 60,
        decoration: BoxDecoration(
            color: isExpanded ? Colors.white : null,
            border:
            isExpanded ? Border.all() : Border.all(style: BorderStyle.none),
            borderRadius: BorderRadius.circular(100)),
        child: Row(
          // mainAxisAlignment: MainAxisAlignment.,
          // mainAxisSize: MainAxisSize.min,
          children: [
            AnimatedSwitcher(
              duration: widget._animationDuration,
              child: isExpanded
                  ? IconButton(
                  onPressed: () {
                    setState(() {
                      isExpanded = false;
                    });
                  },
                  icon: Icon(Icons.search))
                  : FloatingActionButton(
                  onPressed: () {
                    setState(() {
                      isExpanded = !isExpanded;
                    });
                  },
                  child: Icon(Icons.search)),
            ),
            // if (isExpanded)
            if (isExpanded)
              Expanded(
                child: TextField(
                  key: textKey,
                  autofocus: true,
                  decoration: InputDecoration(
                    hintText: 'Suche',
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                  ),
                  onSubmitted: widget.onSubmitted,
                ),
              ),
            if (isExpanded)
              IconButton(
                onPressed: () {
                  setState(() {
                    isExpanded = false;
                  });
                  widget.onSubmitted!("");
                },
                icon: Icon(Icons.clear),
              ),
          ],
        ),
      ),
    );
  }
}

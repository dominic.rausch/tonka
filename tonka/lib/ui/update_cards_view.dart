import 'dart:typed_data';

import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:nfc_manager/nfc_manager.dart';
import 'package:nfc_manager/platform_tags.dart';
import 'package:tonka/data/catalog.dart';
import 'package:tonka/data/catalog_index.dart';
import 'package:tonka/tonuino/tonuino_card.dart';

final MIFARE_CLASSIC_KEY =
    Uint8List.fromList([0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]);

void pushUpdateCardsRoute(BuildContext context, Catalog catalog) {
  Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => UpdateCardsRoute(catalog: catalog)));
}

void _showHelp(BuildContext context) {
  showModalBottomSheet(
    context: context,
    builder: (context) =>
        Container(padding: EdgeInsets.all(16), child: Markdown(data: """
Die Funktion *Karte aktualisieren* versucht, NFC Karten zu korrigieren,
nachdem Dateien auf der SD Karte verschoben wurden. Das kann zum Beispiel nötig
werden, nachdem eine Datei von der SD Karte gelöscht wurde und deshalb die
nachfolgenden Dateien eine andere Nummer haben.

Unterstützte Änderungen:
                                
* Eine Datei oder ein Album wurde innerhalb desselben Ordners eine andere Nummer
  zugewiesen. Zum Beispiel `01/005.mp3` ist nun `01/004.mp3`.
* Der Ordner hat nun eine neue Nummer. Zum Beispiel `01/005.mp3` ist nun 
  `02/005.mp3`.

Es gelten folgende Vorraussetzung für eine erfolgreiche Aktualisierung:

* Die Karte muss mit der Tonka App erstellt worden sein.
* Die Datei, das Album und der Ordner dürfen eine andere Nummer haben, müssen
  aber denselben Namen behalten.

Die Tonka app liest die Karte und sucht nach einer Datei/Ordner/Album mit passendem
Namen im Katalog. Falls es einen Treffer gibt, wird die Karte direkt neu beschrieben.
 """)),
  );
}

class _UpdateCards extends StatefulWidget {
  final SearchableCatalog catalog;

  const _UpdateCards(this.catalog, {Key? key}) : super(key: key);

  @override
  _UpdateCardsState createState() => _UpdateCardsState();
}

class _UpdateCardsState extends State<_UpdateCards> {
  @override
  void initState() {
    NfcManager.instance.stopSession();
    Future nfcFuture =
        NfcManager.instance.startSession(onDiscovered: (NfcTag tag) async {
      var mifareClassic = MifareClassic.from(tag);
      if (mifareClassic == null) {
        debugPrint("Unsupported Tag");
        return;
      }
      try {
        await mifareClassic.authenticateSectorWithKeyA(
            sectorIndex: 1, key: MIFARE_CLASSIC_KEY);
        final tonuinoBytes = await mifareClassic.readBlock(blockIndex: 4);
        final directoryHash = await mifareClassic.readBlock(blockIndex: 5);
        final albumOrTitleHash = await mifareClassic.readBlock(blockIndex: 6);
        final card = TonuinoCard.fromBytes(tonuinoBytes,
            directoryHash: directoryHash, albumOrTitleHash: albumOrTitleHash);
      } catch (e) {
        NfcManager.instance.stopSession(errorMessage: e.toString());
        return;
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    NfcManager.instance.stopSession();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Container(
          height: 100,
          alignment: Alignment.center,
          child: Text(
            "Warte auf Karte...",
            style: Theme.of(context).textTheme.headline6,
          ),
        )
      ],
    );
  }
}

class UpdateCardsRoute extends StatelessWidget {
  final Catalog catalog;

  UpdateCardsRoute({required this.catalog, Key? key}) : super(key: key);

  Future<SearchableCatalog> prepare() async {
    return SearchableCatalog(catalog);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Karten aktualisieren"),
        actions: [
          IconButton(
              onPressed: () => _showHelp(context), icon: Icon(Icons.help))
        ],
      ),
      body: kIsWeb
          ? Container(
              child: Text(
                "Diese Funktion steht in der WebApp nicht zur Verfügung.",
                style: Theme.of(context).textTheme.headline6,
              ),
            )
          : FutureBuilder<SearchableCatalog>(
              future: prepare(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return const Center(child: CircularProgressIndicator());
                } else {
                  return _UpdateCards(snapshot.data!);
                }
              },
            ),
    );
  }
}

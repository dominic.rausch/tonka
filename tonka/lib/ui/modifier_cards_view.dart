import 'package:flutter/material.dart';
import 'package:tonka/tonuino/tonuino_card.dart';
import 'package:tonka/ui/write_nfc_popup.dart';

Widget _makeTile(
    BuildContext context, String label, String subtitle, ModifierType cardType,
    {int? sleepMins}) {
  return ListTile(
    title: Text(label),
    subtitle: Text(
      subtitle,
      maxLines: 2,
    ),
    onTap: () => writeNfcDialog(
        context: context,
        card:
            makeModifierCard(modifierType: cardType, sleepMinutes: sleepMins)),
  );
}

class SleepTimerCardTile extends StatefulWidget {
  const SleepTimerCardTile({Key? key}) : super(key: key);

  @override
  _SleepTimerCardTileState createState() => _SleepTimerCardTileState();
}

class _SleepTimerCardTileState extends State<SleepTimerCardTile> {
  int sleepTimerMins = 60;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: [
          _makeTile(
              context,
              "Schlummermodus",
              "Stopp die Wiedergabe nach $sleepTimerMins Minuten",
              ModifierType.SLEEP_TIMER,
              sleepMins: sleepTimerMins),
          Slider(
              value: sleepTimerMins.toDouble(),
              min: 1,
              max: 255,
              onChanged: (newValue) =>
                  {setState(() => sleepTimerMins = newValue.toInt())},
              label: "$sleepTimerMins")
        ],
      ),
    );
  }
}

class ModifierCardsView extends StatelessWidget {
  ModifierCardsView({Key? key}) : super(key: key);

  Widget _makeCardTile(BuildContext context, String label, String subtitle,
      ModifierType cardType) {
    return Card(child: _makeTile(context, label, subtitle, cardType));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Modifikationskarten erstellen")),
        body: ListView(
          children: [
            _makeCardTile(context, "Admin Karte", "Ruft das Adminmenü auf",
                ModifierType.ADMIN),
            SleepTimerCardTile(),
            _makeCardTile(
                context,
                "Stopptanz",
                "Pausiert die Wiedergabe nach einer zufälligen Zeit und setzt sie wieder fort..",
                ModifierType.FREEZE_DANCE),
            _makeCardTile(
                context,
                "TonUINO Sperre",
                "Nur Adminkarte, Sperrkarte oder neue Karte funktionieren.",
                ModifierType.LOCK),
            _makeCardTile(
                context,
                "Krabblermodus",
                "Tasten des TonUINO werden gesperrt.",
                ModifierType.TODDLER_MODE),
            _makeCardTile(
                context,
                "Kita Modus",
                "Karten funktioniert erst, wenn der aktuelle Titel zuende gespielt wurde.",
                ModifierType.KINDERGARDEN_MODE),
            _makeCardTile(
                context,
                "Wiederholen",
                "Der aktuelle Titel wird endlos wiederholt.",
                ModifierType.REPEAT_SINGLE),
            _makeCardTile(
                context, "Feedback", "Sprachansagen", ModifierType.FEEDBACK),
          ],
        ));
  }
}

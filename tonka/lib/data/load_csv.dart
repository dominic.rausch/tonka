import 'dart:convert';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:tonka/data/csv_row_data.dart';

import 'csv_parser.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

class ImportResult {
  ImportResult(this.csvRows, this.importLog);

  ImportResult.failure(this.importLog) : csvRows = [];

  final List<CsvRowData> csvRows;
  final String importLog;
}

Future<ImportResult> loadCsv() async {
  ImportResult result;
  try {
    result = await _loadCsv();
  } catch (e) {
    return ImportResult.failure(e.toString());
  }
  return result;
}

Future flush() async {
  if (!kIsWeb) {
    FilePicker.platform.clearTemporaryFiles();
  }
}

Future<ImportResult> _loadCsv() async {
  await flush();
  var importLog = StringBuffer();
  FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['.csv', 'csv'],
      allowMultiple: false,
      withData: true);
  if (result == null) {
    importLog.writeln("No file selected.");
    return ImportResult.failure(importLog.toString());
  }
  String? csvContent;
  if (result.files.single.path != null) {
    File file = File(result.files.single.path!);
    csvContent = utf8.decode(file.readAsBytesSync());
  }
  if (csvContent?.isEmpty ?? true && result.files.first.bytes != null) {
    csvContent = utf8.decode(result.files.first.bytes!);
  }

  if (csvContent == null) {
    importLog.writeln("Failed to open file.");
    return ImportResult.failure(importLog.toString());
  }

  importLog.writeln("Loaded ${csvContent.length} bytes");

  flush();

  List<CsvRowData>? rows = parseCsvWithHeader(csvContent, importLog: importLog)
      ?.map((m) => CsvRowData.fromMap(m))
      .toList();
  if (rows == null || rows.isEmpty) {
    importLog.writeln("CSV parsing failed");
    return ImportResult.failure(importLog.toString());
  }
  importLog.writeln("Parsed ${rows.length} rows.");
  return ImportResult(rows, importLog.toString());
}

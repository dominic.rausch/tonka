import 'dart:typed_data';

import 'package:tonka/data/catalog.dart';
import 'package:tonka/tonuino/hash.dart';

class SearchableCatalog {
  var directoryIndex = Map<Uint8List, List<Directory>>();
  var albumIndex = Map<Uint8List, List<Album>>();
  var fileIndex = Map<Uint8List, List<AudioFile>>();

  SearchableCatalog(Catalog catalog) {
    for (Directory directory in catalog.directories) {
      final dirHash = hashString(directory.name);
      if (dirHash == null) continue;

      directoryIndex.putIfAbsent(dirHash, () => []).add(directory);
      for (FileOrAlbum fos in directory.content) {
        if (fos.file != null) {
          final fileHash = hashString(fos.file?.name);
          if (fileHash == null) continue;
          fileIndex.putIfAbsent(fileHash, () => []).add(fos.file!);
        }
        if (fos.dir != null) {
          final dirHash = hashString(fos.dir?.name);
          if (dirHash == null) continue;
          albumIndex.putIfAbsent(dirHash, () => []).add(fos.dir!);
          for (AudioFile file in fos.dir!.files) {
            final fileHash = hashString(fos.file?.name);
            if (fileHash == null) continue;
            fileIndex.putIfAbsent(fileHash, () => []).add(file);
          }
        }
      }
    }
  }

  List<Directory> findDirectory(Uint8List? s) {
    return directoryIndex[s] ?? [];
  }

  List<Album> findAlbum(Uint8List? s) {
    return albumIndex[s] ?? [];
  }

  List<AudioFile> findFile(Uint8List? s) {
    return fileIndex[s] ?? [];
  }
}

import 'package:csv/csv.dart';

List<Map<String, String>>? parseCsvWithHeader(String csv,
    {required StringBuffer importLog}) {
  importLog.writeln("Importing csv with ${csv.length} characters.");
  final eol = csv.contains("\r\n") ? "\r\n" : "\n";
  importLog
      .writeln("Detected newline character: " + (eol == "\n" ? "LF" : "CRLF"));

  List<List<dynamic>> rows = const CsvToListConverter().convert(csv, eol: eol);
  if (rows.isEmpty) {
    importLog.writeln("The csv seems to be empty.");
    return [];
  }

  var header = new Map<int, String>(); // pos -> col name
  rows[0].asMap().forEach((int index, value) {
    header[index] = value;
  });

  List<Map<String, String>> out = [];
  for (int i = 1; i < rows.length; i++) {
    var row = rows[i];
    if (row.length > header.length) {
      importLog.writeln(
          "The CSV's row ${i + 1} has more columns than the header row.");
      return null;
    }
    if (row.length < header.length) {
      importLog.writeln(
          "The CSV's row ${i + 1} has fewer columns than the header row. Skipping!");
      continue;
    }

    var rowAsMap = new Map<String, String>();
    row.asMap().forEach((idx, value) {
      rowAsMap[header[idx]!] = value.toString();
    });
    out.add(rowAsMap);
  }
  return out;
}

import 'csv_row_data.dart';

class Catalog {
  List<Directory> _directories;

  Catalog(this._directories);

  Catalog.empty() : _directories = [];

  Catalog.fromMediaFiles(List<CsvRowData> files) : _directories = [] {
    files.sort((a, b) {
      int byDir = a.directoryNumber.compareTo(b.directoryNumber);
      if (byDir != 0) return byDir;
      return a.fileNumber.compareTo(b.fileNumber);
    });

    for (CsvRowData mediaFile in files) {
      if (_directories.isEmpty ||
          mediaFile.directoryNumber != _directories.last.number) {
        _directories.add(new Directory(
            number: mediaFile.directoryNumber, name: mediaFile.directoryName));
      }
      _directories.last.add(mediaFile);
    }
  }

  List<Directory> get directories => _directories;

  Future<Catalog> filteredCopy(String query) async {
    var filtered = new Catalog.empty();
    filtered._directories = _directories
        .map((d) => d.filteredCopy(query))
        .where((d) => d != null)
        .map((e) => e!)
        .toList(growable: false);
    return filtered;
  }
}

class Directory {
  final int number;
  final String name;
  final List<FileOrAlbum> content = [];

  Directory({required this.number, required this.name});

  void add(CsvRowData mediaFile) {
    if (mediaFile.subdirectoryName.isEmpty) {
      content.add(FileOrAlbum(
          file: AudioFile(
              number: mediaFile.fileNumber,
              name: mediaFile.title,
              directory: this,
              tags: mediaFile.tags,
              runtime: mediaFile.runtime)));
      return;
    }

    if (content.isEmpty ||
        content.last.dir == null ||
        content.last.dir!.name != mediaFile.subdirectoryName) {
      content.add(FileOrAlbum(
          dir: new Album(name: mediaFile.subdirectoryName, directory: this)));
    }
    var album = content.last.dir!;
    album.files.add(new AudioFile(
        number: mediaFile.fileNumber,
        name: mediaFile.title,
        directory: this,
        album: album,
        tags: mediaFile.tags,
        runtime: mediaFile.runtime));
  }

  Directory? filteredCopy(String query) {
    if (name.toLowerCase().contains(query) || number.toString().contains(query))
      return this;

    var newContent = content
        .map((FileOrAlbum e) => e.filteredCopy(query))
        .where((element) => element != null)
        .map((e) => e!);
    if (newContent.isEmpty) return null;

    var filteredDir = Directory(number: number, name: name);
    filteredDir.content.addAll(newContent);
    return filteredDir;
  }
}

class FileOrAlbum {
  final AudioFile? file;
  final Album? dir;

  FileOrAlbum({this.file, this.dir});

  FileOrAlbum? filteredCopy(String query) {
    if (file != null && file!.matches(query)) return this;
    if (dir != null) {
      Album? album = dir!.filteredCopy(query);
      if (album != null) return FileOrAlbum(dir: album);
    }
  }
}

class AudioFile {
  final int number;
  final String name;
  final Directory directory;
  final Album? album;
  final List<String>? tags;
  final Duration? runtime;

  AudioFile(
      {required this.number,
      required this.name,
      required this.directory,
      this.album,
      this.tags = const [],
      this.runtime});

  bool matches(String query) {
    return name.toLowerCase().contains(query) ||
        (tags?.any((tag) => tag.toLowerCase().contains(query)) ?? false) ||
        number.toString().contains(query);
  }
}

class Album {
  final String name;
  final List<AudioFile> files = [];
  final Directory directory;

  Album({required this.name, required this.directory});

  Album? filteredCopy(String query) {
    if (name.toLowerCase().contains(query)) return this;
    var matchedFiles = files.where((element) => element.matches(query));
    if (matchedFiles.isNotEmpty) {
      var album = Album(name: name, directory: directory);
      album.files.addAll(matchedFiles);
      return album;
    }
  }
}

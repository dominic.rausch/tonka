
import 'package:hive/hive.dart';

class DurationHiveTypeAdapter extends TypeAdapter<Duration> {
  @override
  final typeId = 2;

  @override
  Duration read(BinaryReader reader) {
    return Duration(seconds: reader.read());
  }

  @override
  void write(BinaryWriter writer, Duration obj) {
    writer.write(obj.inSeconds);
  }
  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is DurationHiveTypeAdapter &&
              runtimeType == other.runtimeType &&
              typeId == other.typeId;
}
import 'package:hive/hive.dart';

part 'csv_row_data.g.dart';

// This object is used for storing the data in Hive. We don't process it a lot to prevent breaking changes.
@HiveType(typeId: 1)
class CsvRowData extends HiveObject {
  CsvRowData(
      {required this.directoryNumber,
      required this.fileNumber,
      required this.title,
      required this.subdirectoryName,
      required this.directoryName,
      this.runtime,
      this.tags});

  factory CsvRowData.fromMap(Map<String, String> map) {
    var mediaFile = CsvRowData(
        directoryNumber: int.parse(map["directoryNumber"].toString()),
        fileNumber: int.parse(map["fileNumber"].toString()),
        title: _toString(map["title"]),
        subdirectoryName: _toString(map["subdirectoryName"]),
        directoryName: _toString(map["directoryName"]),
        runtime: _parseDuration(map["runtime"] ?? ""),
        tags: _splitTags(map["tags"]));
    if (mediaFile.directoryNumber < 1 || mediaFile.directoryNumber > 99) {
      throw FormatException(
          "Ungültige Ordnernummer (${mediaFile.directoryNumber}, erlaubt 1-99)");
    }
    if (mediaFile.fileNumber < 1 || mediaFile.fileNumber > 255) {
      throw FormatException(
          "Ungültige Dateinummer (${mediaFile.fileNumber}, erlaubt 1-255)");
    }

    return mediaFile;
  }

  static String _toString(dynamic d) {
    if (d == null) return "";
    return d.toString();
  }

  static Duration? _parseDuration(String s) {
    int? seconds = int.tryParse(s);
    return seconds == null ? null : Duration(seconds: seconds);
  }

  static List<String>? _splitTags(String? tags) {
    final list =
        tags?.toString().split(";;").where((String s) => s.isNotEmpty).toList();
    return (list?.isEmpty ?? true) ? null : list;
  }

  @HiveField(0)
  int directoryNumber;

  @HiveField(1)
  int fileNumber;

  @HiveField(2)
  String title;

  @HiveField(3)
  String subdirectoryName;

  @HiveField(4)
  String directoryName;

  @HiveField(5)
  List<String>? tags;

  @HiveField(6)
  Duration? runtime;
}

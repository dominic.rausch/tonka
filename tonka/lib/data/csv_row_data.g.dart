// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'csv_row_data.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CsvRowDataAdapter extends TypeAdapter<CsvRowData> {
  @override
  final int typeId = 1;

  @override
  CsvRowData read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CsvRowData(
      directoryNumber: fields[0] as int,
      fileNumber: fields[1] as int,
      title: fields[2] as String,
      subdirectoryName: fields[3] as String,
      directoryName: fields[4] as String,
      runtime: fields[6] as Duration?,
      tags: (fields[5] as List?)?.cast<String>(),
    );
  }

  @override
  void write(BinaryWriter writer, CsvRowData obj) {
    writer
      ..writeByte(7)
      ..writeByte(0)
      ..write(obj.directoryNumber)
      ..writeByte(1)
      ..write(obj.fileNumber)
      ..writeByte(2)
      ..write(obj.title)
      ..writeByte(3)
      ..write(obj.subdirectoryName)
      ..writeByte(4)
      ..write(obj.directoryName)
      ..writeByte(5)
      ..write(obj.tags)
      ..writeByte(6)
      ..write(obj.runtime);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CsvRowDataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

# See the README for instructions on how to use this script.

import argparse
import csv
import os
import pathlib
import re
import sys

from enum import Enum
from collections import namedtuple
from shutil import copyfile


# The columns of the csv.
csv_cols = ['directoryNumber', 'fileNumber', 'title', 'subdirectoryName', 'directoryName', 'tags', 'runtime']

# A type representing one row of the csv.
CatalogEntry =namedtuple('CatalogEntry', csv_cols)

# The catalog of all csv rows.
catalog_rows = []

# A type representing one file on disk.
FileHandle = namedtuple('FileHandle', 'number name is_mp3 path')


# Adds a single row to the in-memory catalog (to be ouput later)
def write_to_catalog(d, f, dir_name, subdir_name, file_name, file_num):
  catalog_rows.append(
    CatalogEntry(
      directoryNumber = str(d), 
      fileNumber = str(f), 
      title = file_name, 
      subdirectoryName=subdir_name, 
      directoryName = dir_name, 
      # TODO: Add support for tags and runtime.
      tags = "",
      runtime = ""))


# Writes the in-memory catalog to disk.
def write_catalog_to_file(out_path):
  with open(out_path/'catalog.csv', mode = 'w', encoding='utf-8') as csv_file:
    csv_writer = csv.writer(csv_file)
    csv_writer.writerow(csv_cols)
    for row in catalog_rows:
      csv_writer.writerow([getattr(row, col) for col in csv_cols]) 

def split_name_and_number(s):

  date_match = re.match(r'(\d\d\d\d-\d\d-\d\d)[\.\s\-_]+(.*)', s)
  if date_match:
    date_match.group(1).replace('-','')
    return (int(date_match.group(1).replace('-','')), date_match.group(2))

  m = re.match(r'(\d+)[\.\s\-_]+(.*)', s)
  if not m:
    return None
  return (int(m.group(1)), m.group(2))

def make_file_handle(path):
  (dir_num, dir_name) = split_name_and_number(path.stem) or (None, None)
  return FileHandle(number=dir_num, name=dir_name, path=path, is_mp3 = (path.suffix.lower() == '.mp3' and not path.is_dir()))


def GetAllFiles(path):
  return [make_file_handle(d) for d in path.iterdir()]

def CopyOrSymlink(src, dst, symlink):
  if symlink:
    os.symlink(os.path.relpath(src, start=dst.parent), dst)
  else:
    copyfile(src, dst)


def handleTopLevelDir(input_dir, out, compress, symlink):
  assert (input_dir.number > 0 and input_dir.number < 100), 'Ordner müssen von 01 bis 99 nummeriert sein'

  all_files = [d for d in GetAllFiles(input_dir.path) if d.number is not None]
  all_files.sort(key=lambda x: (x.number, x.name))

  sd_dir = input_dir.path.parent / out / str(input_dir.number).zfill(2)
  if all_files:
    sd_dir.mkdir(parents=True, exist_ok=True)

  print(f'Processing {input_dir.number}: {input_dir.name}')

  # The number that was extracted from the file name of the last written file.
  last_seen_index = 0

  # The number of the next file to be written.
  compressed_index = 1
  for f in all_files:
    if not (f.path.is_dir() or f.is_mp3):
      continue

    # assert(f.number > last_seen_index), f"{f.path.as_posix()} has a duplicate file index"
    if compress:
      assert(compressed_index < 255), f"Found more than 255 files in {input_dir.path}"

    if f.path.is_dir():
      album_files = [d for d in GetAllFiles(f.path) if d.is_mp3 and d.number is not None]
      album_files.sort(key=lambda x: (x.number, x.name))

      if not compress:
        assert (len(album_files) + f.number <= 256), f"{f.path.as_posix()} has too many files to fit into the 255 files limit"
      for album_idx, album_file in enumerate(album_files):
        out_file_number = f.number + album_idx if not compress else compressed_index
        sd_file = (sd_dir / str(out_file_number).zfill(3)).with_suffix('.mp3')

        CopyOrSymlink(album_file.path, sd_file, symlink)
        # os.symlink(os.path.relpath(album_file.path, start=sd_dir), sd_file)
        
        last_seen_index = f.number + len(album_files) - 1
        compressed_index = compressed_index +1

        write_to_catalog(d=input_dir.number, f=out_file_number, dir_name=input_dir.name, subdir_name=f.name, file_name=album_file.name, file_num=album_file.number)
    if f.is_mp3:
      if not compress:
        assert  (f.number < 256), f'File numbers must be 1-255. Found {f.number}.'
      out_file_number = f.number if not compress else compressed_index
      sd_file = (sd_dir / str(out_file_number).zfill(3)).with_suffix('.mp3')

      CopyOrSymlink(f.path, sd_file, symlink)
      # os.symlink(os.path.relpath(f.path, start=sd_dir), sd_file)

      last_seen_index = f.number
      compressed_index = compressed_index +1

      write_to_catalog(d=input_dir.number, f=out_file_number, dir_name=input_dir.name, subdir_name=None, file_name=f.name, file_num=f.number)
      
  
  print(f"Wrote {compressed_index-1} files.")


    


def SelectOutPath(path):
  if not (path / 'sd').exists():
    return 'sd'
  idx = 1
  while (path / f'sd{idx}').exists():
    idx = idx + 1
  return f'sd{idx}'


def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('--source', help='Path to the directory containing all the mp3s. Uses `.` when not set', default='.', type=pathlib.Path)
  parser.add_argument('--out', help='Where to write the tonuino sd card.', type=pathlib.Path)
  parser.add_argument('--compress', help='If true, the script will fill holes in the output.', type=bool, default=True)
  parser.add_argument('--symlink', help='If true, the script will create symlinks instead of hard copies.', type=bool, default=False)

  args = parser.parse_args()

  cwd = pathlib.Path(args.source).resolve()
  print(f"Scanning {cwd}.")
  
  tonuino_dirs = [d for d in GetAllFiles(pathlib.Path(cwd)) if d.path.is_dir() and d.number is not None]
  tonuino_dirs.sort(key=lambda x: x.number)
  
  # Assert that dir numbers are unique.
  assert (len(tonuino_dirs) == len({d.number for d in tonuino_dirs})), 'There are multiple directories with the same number.'

  out_path = args.out or SelectOutPath(cwd)
  print(f"Populating result to '{out_path}'")

  for dir in tonuino_dirs:
    handleTopLevelDir(dir, out_path, args.compress, args.symlink)    
    
  print("Done writing files. Creating Catalog")
  write_catalog_to_file(pathlib.Path(cwd) / out_path)



if __name__ == "__main__":
  assert sys.version_info >= (3, 8), "This script requires Python 3.8 or greater."
  main()
